package main

import (
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func errHandling(err error) {
	log.Println(err)
	os.Exit(2)
}

type Wallpaper struct {
	ImageRaw []byte
	Name     string
	Path     string
	Size     string
}

func (self *Wallpaper) getName() {
	// <meta property="og:image" content="https://www.bing.com/th?id=OHR.${IMAGE_ID}_tmb.jpg&amp;rf=" />
	resp, err := http.Get("http://bing.com")
	if err != nil {
		errHandling(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errHandling(err)
	}
	html := string(body)
	imageID := html[strings.Index(html, "th?id=OHR.")+len("th?id=") : strings.Index(html, "_tmb.jpg")]
	self.Name = strings.Join([]string{imageID, "_", self.Size, ".jpg"}, "")
}

func (self *Wallpaper) getImage() {
	// https://www.bing.com/th?id=OHR.${IMAGE_ID}_${IMAZE_SIZE}.jpg
	// IMAGE_SIZE: 1920x1080(default) or UHD
	imageURL := strings.Join([]string{"https://www.bing.com/th?id=", self.Name}, "")
	resp, err := http.Get(imageURL)
	if err != nil {
		errHandling(err)
	}
	if resp.StatusCode != 200 {
		log.Println("Failed to get " + imageURL)
		os.Exit(2)
	}
	defer resp.Body.Close()
	imageRaw, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errHandling(err)
	}
	self.ImageRaw = imageRaw
}

func (self *Wallpaper) Save() {
	outPath := strings.Join([]string{self.Path, self.Name}, "")
	file, err := os.Create(outPath)
	defer file.Close()
	if err != nil {
		errHandling(err)
	}
	file.Write(self.ImageRaw)
}

func (self *Wallpaper) Set() {
	switch runtime.GOOS {
	case "darwin":
		path := self.Path + self.Name
		err := exec.Command("osascript", `-e tell application "System Events" to set picture of (reference to every desktop) to`, path).Run()
		if err != nil {
			errHandling(err)
		}
	case "linux":
		switch os.Getenv("XDG_CURRENT_DESKTOP") {
		case "GNOME":
			path := `'` + "file://" + self.Path + self.Name + `'`
			err := exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-uri", path).Run()
			err = exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-uri-dark", path).Run()
			if err != nil {
				errHandling(err)
			}
		}
	}
}

func main() {
	var defaultPath string = os.Getenv("HOME") + "/.cache/wfb/"
	_, err := os.Stat(defaultPath)
	// init cache directory
	if err != nil && os.IsNotExist(err) {
		err := os.MkdirAll(defaultPath, 0755)
		if err != nil {
			errHandling(err)
		}
	}
	var (
		size   = flag.String("size", "1920x1080", "--size [UHD]")
		outDir = flag.String("out", defaultPath, "--out [/path/to/output/dir/]")
		noSet = flag.Bool("noset", false, "--noset: download only")
	)
	flag.Parse()
	// /path/to/output/dir => /path/to/output/dir/
	if !strings.HasSuffix(*outDir, "/") {
		*outDir = strings.Join([]string{*outDir, "/"}, "")
	}
	wallpaper := &Wallpaper{
		Path: *outDir,
		Size: *size,
	}
	wallpaper.getName()
	wallpaper.getImage()
	wallpaper.Save()
	if !*noSet {
		wallpaper.Set()
	}
}
