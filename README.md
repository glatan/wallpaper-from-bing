# Wfb

Wallpaper from Bing

## Status

* [x] GNOME
* [ ] macOS

## Usage

```bash
# Download a wallpaper to the cache directory and set.
$ go run wfb.go
# Download an UHD wallpaper to the cache directory and set.
$ go run wfb.go --size UHD
# Download a wallpaper to /path/to/directory and set.
$ go run wfb.go --out /path/to/directory
```
