SHELL    := /usr/bin/env bash
NAME      = wfb
LDFLAGS   = -tags netgo -installsuffix netgo -ldflags '-w -s -extldflags "-static"' -buildmode=pie

default: build

.PHONY: build
build:
	@go build ${LDFLAGS} -o ${NAME}

.PHONY: fmt
fmt:
	@go fmt

.PHONY: vet
vet:
	@go vet
.PHONY: test
test:
	@go test

.PHONY: test/dl
test/dl:
	@go run ${NAME}.go --out ${PWD} --noset
